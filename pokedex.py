# API Based Pokédex Application
# Author: Trey Phillips

import json
import requests
from graphics import *
import os


class Button:
    # Class representing a graphic button

    def __init__(self, window, text, anchor, height, width, color):
        """
        Initializes a Button on the given window with a given label at a given anchor of a given
            height, width, and color.
        @param window: GraphWin Object
        @param text: String
        @param anchor: Graphics Point Object
        @param height: Int
        @param width: Int
        @param color: String representing a color
        """

        self.ulx = anchor.getX() - 0.5 * width
        self.uly = anchor.getY() + 0.5 * height
        self.lrx = anchor.getX() + 0.5 * width
        self.lry = anchor.getY() - 0.5 * height
        self.pressed = False
        self.text = Text(anchor, text)
        self.shape = Rectangle(Point(self.ulx, self.uly), Point(self.lrx, self.lry))
        self.shape.setFill(color)
        self.shape.draw(window)
        self.text.setSize(20)
        self.text.draw(window)

    def is_in(self, point):
        """
        Takes a point and returns if that point falls in this button object.
        @param point: Graphics Point Object
        @return: Boolean regarding if the point is within the object
        """
        return self.ulx < point.getX() < self.lrx and self.lry < point.getY() < self.uly


class Pokemon:
    # Class representing a Pokemon that stores all the information relative to such.

    def __init__(self, dex_entry):
        """
        Initializes Pokemon from a given dex_entry.
        @param dex_entry: Dictionary Object obtained from pokeAPI
        """
        self.name = dex_entry["name"]
        self.id = dex_entry["id"]
        self.types = []
        for type_entry in dex_entry["types"]:
            self.types.append(type_entry["type"]["name"])
        self.abilities = []
        for ability in dex_entry["abilities"]:
            self.abilities.append(ability["ability"]["name"])
        self.weight = dex_entry["weight"]
        self.height = dex_entry["height"]
        self.text = Text(Point(300, 340), str(self))
        self.text.setSize(30)
        self.text.setTextColor("White")
        self.text.setFace("courier")
        self.sprite_url = dex_entry["sprites"]["front_default"]
        self.get_sprite()
        self.sprite_img = Image(Point(100, 150), "current_sprite.png")
        self.sprite_box = Rectangle(Point(51, 100), Point(148, 200))
        self.sprite_box.setFill("#dddddd")

    def __repr__(self):
        # @return: String Representation of a compilation of the Pokemon's Attributes

        retstring = ""
        # Name
        retstring += self.name_string()
        # Number
        retstring += self.number_string()
        # Types
        retstring += self.types_string()
        # Abilities
        retstring += self.abilities_string()
        # Height
        retstring += self.height_string()
        # Weight
        retstring += self.weight_string()

        return retstring

    # @return: String representation of the Pokemon's Name
    name_string = lambda self: self.name.capitalize()

    # @return: String representation of the Pokemon's Pokedex Number
    number_string = lambda self: f"\nDex No. {self.id}"

    # @return: String representation of the Pokemon's Height
    height_string = lambda self: "Height: " + str(self.height / 10) + "m\n"

    # @return: String representation of the Pokemon's Weight
    weight_string = lambda self: "Weight: " + str(self.weight / 10) + "kg\n"

    def types_string(self):
        # @return: String Representation of the Pokemon's type(s)

        retstring = "\n"
        if len(self.types) > 1:
            retstring += "Types: "
        else:
            retstring += "Type: "
        for type_string in self.types:
            retstring += type_string.capitalize() + "-"
        retstring = retstring[:-1] + "\n"
        return retstring

    def abilities_string(self):
        # @return: String Representation of the Pokemon's ability/ies

        retstring = ""
        if len(self.abilities) > 1:
            retstring += "Abilities: "
        else:
            retstring += "Ability: "
        for index in range(len(self.abilities)):
            if index == 0:
                if len(self.abilities) > 2:
                    retstring += self.abilities[index].capitalize() + ",\n"
                else:
                    retstring += self.abilities[index].capitalize() + "\n"
            elif index == (len(self.abilities) - 1):
                if len(self.abilities) > 2:
                    retstring += "\nand " + self.abilities[index].capitalize() + "\n"
                else:
                    retstring += "and " + self.abilities[index].capitalize() + "\n"
            else:
                retstring += self.abilities[index].capitalize() + ","
        return retstring

    def draw(self, window):
        """
        Draws this Pokemon's data upon the given window.
        @param window: GraphWin Object
        @return: Nothing, called to draw the Pokemon's attributes upon the given window
        """
        self.text.draw(window)
        self.sprite_box.draw(window)
        self.sprite_img.draw(window)

    def update(self, dex_entry, window):
        """
        Refreshes this Pokemon's data to fit that of a given dex_entry and draw's the updated
            version to a given window.
        @param dex_entry: Dictionary Object obtained from API
        @param window: GraphWin Object
        @return: Nothing, called to update parameters given to the Pokemon Object and update the
            graphical representation
        """
        self.text.undraw()
        self.sprite_img.undraw()
        self.__init__(dex_entry)
        self.draw(window)

    def get_sprite(self):
        response = requests.get(self.sprite_url)
        current_sprite = open("current_sprite.png", "wb")
        current_sprite.write(response.content)
        current_sprite.close()


class Pokedex:
    def get_entry(self, pokemon_str):
        """
        Retrieves entry of the Pokemon with the given identifier from pokeAPI.
        @param pokemon_str: String of a Pokemon's Name or Dex Number
        @return: Dictionary Object obtained from API containing attributes regarding the given Pokemon
        """
        target = "https://pokeapi.co/api/v2/pokemon/" + str(pokemon_str).lower()
        entry = requests.get(target)
        return entry.json()

    def op_loop(
        self, pokemon, window, search_field, go_button, intro_text, done_button
    ):
        """
        Runs the Pokedex for queries until done is clicked or the window is closed.
        @param window: GraphWin Object
        @param search_field: Graphics Entry Object
        @param go_button: Button Object
        @param intro_text: String of introductory text
        @param close_button: Button Object
        @return: Nothing, called to sustain the application for however long the user wishes to use it
        """
        while window.isOpen():
            try:
                click = window.getMouse()
            except AttributeError and GraphicsError:
                break
            else:
                if go_button.is_in(click):
                    intro_text.undraw()
                    if pokemon is None:
                        try:
                            pokemon = Pokemon(self.get_entry(search_field.getText()))
                            pokemon.draw(window)
                            search_field.setText("")
                        except json.decoder.JSONDecodeError:
                            print(
                                "We couldn't find that Pokémon in our database,\ncheck your spelling and try again!"
                            )
                    else:
                        try:
                            pokemon.update(
                                self.get_entry(search_field.getText()), window
                            )
                            search_field.setText("")
                        except json.decoder.JSONDecodeError:
                            print(
                                "We couldn't find that Pokémon in our database,\ncheck your spelling and try again!"
                            )
                elif window.isOpen() and done_button.is_in(click):
                    window.close()

        if os.path.exists("current_sprite.png"):
            os.remove("current_sprite.png")

    def graph_loop(self):
        # Initializes the GUI and starts the op_loop of this Pokedex.

        pokemon = None
        pokedex_win = GraphWin("Pokédex", 600, 600)
        pokedex_win.setBackground("Red")
        go_button = Button(pokedex_win, "Search", Point(525, 25), 40, 80, "#60ff60")
        search_field = Entry(Point(260, 25), 30)
        search_field.setFace("courier")
        search_field.setSize(20)
        search_field.setFill("White")
        search_field.draw(pokedex_win)
        intro_text = Text(
            Point(300, 300), "Hello, which pokemon would\nyou like to know more about?"
        )
        intro_text.setSize(25)
        intro_text.setFill("White")
        intro_text.draw(pokedex_win)
        done_button = Button(pokedex_win, "Done", Point(550, 550), 100, 100, "#0bf")
        self.op_loop(
            pokemon, pokedex_win, search_field, go_button, intro_text, done_button
        )

def main():
    pokedex = Pokedex()
    pokedex.graph_loop()

if __name__ == "__main__":
    main()
